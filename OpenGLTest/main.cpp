#include <iostream>
#define GLEW_STATIC
#include <GL/glew.h>
#include "shader.hpp"
#include <GLFW/glfw3.h>
#include "stb_image.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Square.hpp"

const GLint width = 800;
const GLint height = 600;


//  Camera Variables
glm::vec3 cameraPosition = glm::vec3(0.0f, 0.3f, 6.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
//   Behaviour Variables
float lastFrame = 0.0f;
float delta_Time = 0.0f;
float cameraSpeed = 0.5f;

void setLights(Shader, float, float, float, float);
void setMatrices(Shader, glm::mat4, glm::mat4, glm::mat4);

void processInput(GLFWwindow *window)
{
    cameraSpeed = 1.0f * delta_Time;
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        cameraPosition += cameraFront * cameraSpeed;
    
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        cameraPosition -= cameraFront * cameraSpeed;
    
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        cameraPosition -= glm::normalize(glm::cross(cameraFront, up)) * cameraSpeed;
    
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cameraPosition += glm::normalize(glm::cross(cameraFront, up)) * cameraSpeed;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
using namespace glm;
int main(){
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    GLFWwindow *window = glfwCreateWindow(width, height, "Window", nullptr, nullptr);
    
    int screenWidth, screenHeight;
    
    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    
    if(nullptr==window){
        std::cout<<"Failed to create GLFW Window";
        glfwTerminate();
        return -1;
    }
    
    glfwMakeContextCurrent(window);
    glewExperimental = GL_TRUE;
    
    if(GLEW_OK != glewInit()){
        std::cout<<"Failed to initialize GLEW";
        return -1;
    }
    
    glViewport(0, 0, screenWidth, screenHeight);
    
    glEnable(GL_DEPTH_TEST);
    Shader shaderProgram("/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/shader.vs","/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/shader.fs");
    
    Shader lightningShader("/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/lightShader.vs","/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/lightShader.fs");
    
    /*  Texturas
     */
    
    //  Arreglo que guarda las tres texturas
    GLuint textures[3];
    glGenTextures(3, textures);
    
    int w, h, c;
    unsigned char* image;
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE0, textures[0]);
    
    
    // Para cargar una textura
    image = stbi_load("/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/textures/red.jpg", &w, &h, &c, 0);
    
    // Para generar una textura
    //glBindTexture(GL_TEXTURE_2D, texture);
    if(image){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        glGenerateMipmap(GL_TEXTURE_2D);
    }else{
        std::cout<<"Failed to load texture";
    }
    stbi_image_free(image);
    glUniform1i(glGetUniformLocation(shaderProgram.ID, "tex"), 0);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Segunda textura
    glActiveTexture(GL_TEXTURE1);
    image = stbi_load("/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/textures/checkerboard.jpg", &w, &h, &c, 0);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    if(image){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        glGenerateMipmap(GL_TEXTURE_2D);
    }else{
        std::cout<<"Failed to load texture";
    }
    stbi_image_free(image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Tercera textura
    glActiveTexture(GL_TEXTURE2);
    image = stbi_load("/Users/luiscorrea/Google Drive/Semestre VI/Programación Gráfica/Parcial I/OpenGLTest/OpenGLTest/textures/box.jpg", &w, &h, &c, 0);
    glBindTexture(GL_TEXTURE_2D, textures[2]);
    if(image){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        //glGenerateMipmap(GL_TEXTURE_2D);
    }else{
        std::cout<<"Failed to load texture";
    }
    stbi_image_free(image);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Geometría vertices(3), normales(3), coordenadas de textura (2)
    float first[] = {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 1.0f,
        // 5
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 0.0f,
        // 11
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,   1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,   0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,   0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,   0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,   1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,   1.0f, 1.0f,
        // 17
        0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
        0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
        0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
        0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        // 23 - Floor
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,   0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,   0.0f, 1.0f,
        // 29
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,   0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,   0.0f, 1.0f
        // 35
    };
    
    // Los cubos tienen las normales dentro
    float cubes[] = {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   0.0f, 0.0f,
        0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 0.0f,
        0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,   1.0f, 1.0f,
        // 5
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    0.0f, 0.0f,
        0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 1.0f,
        0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,    1.0f, 0.0f,
        // 11
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,   1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,   0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,   0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,   0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,   1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,   1.0f, 1.0f,
        // 17
        0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
        0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
        0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
        0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        // 23 - Floor
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,   0.0f, 1.0f,
        0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 1.0f,
        0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 0.0f,
        0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,   1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,   0.0f, 1.0f,
        // 29
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,   0.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 1.0f,
        0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,   1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,   0.0f, 1.0f
        // 35
    };
    
    unsigned int idx[] = {
        6, 7, 8, 9, 10, 11,
        12, 13, 14, 15, 16, 17,
        18, 19, 20, 21, 22, 23,
        30, 31, 32, 33, 34, 35,
        // Piso
        24, 25, 26, 27, 28, 29
        
    };
    // Needed elements for drawing
    unsigned int VBO, VAO, EBO;
    
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(first), first, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idx), idx, GL_STATIC_DRAW);
    
    // Atributo para los
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) 0);
    // Atributo para la normal
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    // Atributo para la textura
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    
    glm::vec3 cubePositions[] = {
        glm::vec3(2.0f, 0.0f, -3.0f),
        glm::vec3(-2.0f, 0.0f,  -4.0f),
        glm::vec3(0.f, 0.5f, -3.0f),
        glm::vec3(4.5f, 1.5f, -4.5f)
    };
    
    
    shaderProgram.use();
    GLuint modelLoc = glGetUniformLocation(shaderProgram.ID, "model");
    GLuint viewLoc = glGetUniformLocation(shaderProgram.ID, "view");
    GLuint projLoc = glGetUniformLocation(shaderProgram.ID, "projection");
    
    //glGetUniformLocation
    //shaderProgram.use();
    lightningShader.use();
    
    
    lightningShader.use();
    lightningShader.setInt("material.diffuse", 2);
    
    
    
    /*
        CICLO DE PROGRAMA
     */
    while(!glfwWindowShouldClose(window)){
        glfwPollEvents();
        processInput(window);
        
        // render
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //glClear(GL_COLOR_BUFFER_BIT);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Se utiliza para que el movimiento sea según cada frame,
        // contando por el tiempo que se pierde en generarlo. Para el
        // movimiento de la cámara
        float currentFrame = glfwGetTime();
        delta_Time = currentFrame - lastFrame;
        lastFrame = currentFrame;
        
        //  Modificaciones del modelo en espacio local
        glm::mat4 model;
        //  Modificaciones del modelo en espacio mundo, translate
        glm::mat4 view;
        //  Proyección de cámara
        glm::mat4 projection;
        
        
        //model = glm::rotate(model, glm::radians(currentFrame*10), glm::vec3( 1.0f, 1.0f, 0.0f ));
        model = mat4(1.0f);
        model = glm::scale(model, glm::vec3( 10.0f, 4.0f, 10.0f ));
        //model = glm::translate(model, glm::vec3( 0.0f, 0.8f, 0.0f ));
        view  = glm::lookAt(cameraPosition, cameraPosition + cameraFront, up);
        projection = glm::perspective(glm::radians( 45.0f ), (float)screenWidth / (float)screenHeight, 3.0f, 100.0f);
        
        lightningShader.use();
        lightningShader.setMat4("model", model);
        lightningShader.setMat4("view", view);
        lightningShader.setMat4("projection", projection);
        
        //Dibuja las 5 paredes del cuarto
        glBindVertexArray(VAO);
        //glUniform1i(glGetUniformLocation(lightningShader.ID, "tex"), 0);
        lightningShader.setInt("material.diffuse", 0);
        glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, 0);
        
        //Dibuja el piso con textura de cuadros
        //glUniform1i(glGetUniformLocation(lightningShader.ID, "tex"), 1);
        lightningShader.setInt("material.diffuse", 1);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)(24 * sizeof(GLuint)));
        
        lightningShader.use();
        //Poner todo lo relacionado a este shader aquí
        lightningShader.setVec3("viewPos",cameraPosition);
        lightningShader.setFloat("material.shininess", 30.0f);
        
        setLights(lightningShader, 0.5f, 0.5f, 0.22f, 0.20f);
        // Ciclo for que dibuja los tres cubos
        for(int i=0; i<3; i++){
            // Traslada el modelo en el espacio
            model = glm::mat4(1.0f);
            model = glm::translate(model, cubePositions[i]);
            
            //Asigna textura de caja
            lightningShader.setInt("material.diffuse", 2);
            
            lightningShader.setMat4("model", model);
            lightningShader.setMat4("view", view);
            lightningShader.setMat4("projection", projection);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        glfwSwapBuffers(window);
        glfwPollEvents();
        
    }
    
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    glfwTerminate();
}

void setLights(Shader lightningShader, float ambient, float diffuse, float linear, float quadratic){
    // Setea todas las variables de la struct pointLights
    glm::vec3 pointLightPositions[] = {
        glm::vec3(4.4f, 1.5f, -4.5f),
        glm::vec3(-4.4f, 1.5f,  -4.5f),
        glm::vec3(-4.4f, -1.5f, -4.5f)
    };
    
    /*lightningShader.setVec3("pointLights[0].position", pointLightPositions[0]);
    lightningShader.setVec3("pointLights[0].ambient", ambient, ambient, ambient);
    lightningShader.setVec3("pointLights[0].diffuse", diffuse, diffuse, diffuse);
    lightningShader.setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
    lightningShader.setFloat("pointLights[0].constant", 1.0f);
    lightningShader.setFloat("pointLights[0].linear", linear);
    lightningShader.setFloat("pointLights[0].quadratic", quadratic);
    
    lightningShader.setVec3("pointLights[1].position", pointLightPositions[1]);
    lightningShader.setVec3("pointLights[1].ambient", ambient, ambient, ambient);
    lightningShader.setVec3("pointLights[1].diffuse", diffuse, diffuse, diffuse);
    lightningShader.setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
    lightningShader.setFloat("pointLights[1].constant", 1.0f);
    lightningShader.setFloat("pointLights[1].linear", linear);
    lightningShader.setFloat("pointLights[1].quadratic", quadratic);
    
    lightningShader.setVec3("pointLights[2].position", pointLightPositions[2]);
    lightningShader.setVec3("pointLights[2].ambient", ambient, ambient, ambient);
    lightningShader.setVec3("pointLights[2].diffuse", diffuse, diffuse, diffuse);
    lightningShader.setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
    lightningShader.setFloat("pointLights[2].constant", 1.0f);
    lightningShader.setFloat("pointLights[2].linear", linear);
    lightningShader.setFloat("pointLights[2].quadratic", quadratic);*/
    
    // point light 1
    lightningShader.setVec3("pointLights[0].position", pointLightPositions[0]);
    lightningShader.setVec3("pointLights[0].ambient", 0.05f, 0.05f, 0.05f);
    lightningShader.setVec3("pointLights[0].diffuse", 0.8f, 0.8f, 0.8f);
    lightningShader.setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
    lightningShader.setFloat("pointLights[0].constant", 1.0f);
    lightningShader.setFloat("pointLights[0].linear", 0.09);
    lightningShader.setFloat("pointLights[0].quadratic", 0.032);
    // point light 2
    lightningShader.setVec3("pointLights[1].position", pointLightPositions[1]);
    lightningShader.setVec3("pointLights[1].ambient", 0.05f, 0.05f, 0.05f);
    lightningShader.setVec3("pointLights[1].diffuse", 0.8f, 0.8f, 0.8f);
    lightningShader.setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
    lightningShader.setFloat("pointLights[1].constant", 1.0f);
    lightningShader.setFloat("pointLights[1].linear", 0.09);
    lightningShader.setFloat("pointLights[1].quadratic", 0.032);
    // point light 3
    lightningShader.setVec3("pointLights[2].position", pointLightPositions[2]);
    lightningShader.setVec3("pointLights[2].ambient", 0.05f, 0.05f, 0.05f);
    lightningShader.setVec3("pointLights[2].diffuse", 0.8f, 0.8f, 0.8f);
    lightningShader.setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
    lightningShader.setFloat("pointLights[2].constant", 1.0f);
    lightningShader.setFloat("pointLights[2].linear", 0.09);
    lightningShader.setFloat("pointLights[2].quadratic", 0.032);
}
