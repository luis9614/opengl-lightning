#version 330 core
out vec4 FragColor;
in vec2 TexCoord;

uniform sampler2D tex;


void main()
{
	//FragColor = texture(tex,TexCoord) * vec4(myColor, 1.0f);
    vec4 colCheck = texture(tex, TexCoord);
    //FragColor = mix(colWood, colCheck, var);
    FragColor = colCheck;
}
