//
//  Square.hpp
//  OpenGLTest
//
//  Created by Luis Correa Morán on 4/3/18.
//  Copyright © 2018 LC. All rights reserved.
//

#ifndef Square_hpp
#define Square_hpp

#include <stdio.h>
#include <algorithm>
class Square{
public:
    int size = 30;
    float* VAO;
    float* VBO;
    float* vertex;
    Square(float px, float py, float plength){
        float aux_vertex[30] = {
                    px, py, 0.0f,                   px, py,
                    px, py + plength, 0.0f,         px, py+plength,
                    px+plength, py, 0.0f,           px + plength, py,
            
                    px + plength, py+plength, 0.0f, px + plength, py + plength,
                    px, py + plength, 0.0f,         px, py+plength,
                    px+plength, py, 0.0f,           px + plength, py
        };
        std::copy(aux_vertex, aux_vertex + 30, vertex);
    }
    
    float* getArrayVertex(){
        return vertex;
    }
};
#endif /* Square_hpp */
